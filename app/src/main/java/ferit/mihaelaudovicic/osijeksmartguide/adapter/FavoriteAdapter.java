package ferit.mihaelaudovicic.osijeksmartguide.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ferit.mihaelaudovicic.osijeksmartguide.DBFavHelper;
import ferit.mihaelaudovicic.osijeksmartguide.R;
import ferit.mihaelaudovicic.osijeksmartguide.model.FavoritesData;
import ferit.mihaelaudovicic.osijeksmartguide.model.RecentsData;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder> {

    Context context;
    List<FavoritesData> favDataList; //?
    DBFavHelper FavDB;

    public FavoriteAdapter(Context context, List<FavoritesData> favDataList) {
        this.context = context;
        this.favDataList = favDataList;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_fav_user,parent,false);
        FavDB=new DBFavHelper(context);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.place_name.setText(favDataList.get(position).getPlaceName());
        holder.place_image.setImageResource(favDataList.get(position).getImageUrl());

    }

    @Override
    public int getItemCount() {
        return favDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView place_image;
        TextView place_name;
        ImageView favBtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            place_image=itemView.findViewById(R.id.favName);
            place_name=itemView.findViewById(R.id.favImage);
            favBtn=itemView.findViewById(R.id.favBtn);

            favBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    final FavoritesData favoritesItem = favDataList.get(position);
                    FavDB.removeFav(favoritesItem.getId());
                    removeItem(position);
                }
            });
        }
    }
    private void removeItem(int position){
        favDataList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, favDataList.size());
    }
}
