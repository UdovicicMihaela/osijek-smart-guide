package ferit.mihaelaudovicic.osijeksmartguide.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ferit.mihaelaudovicic.osijeksmartguide.DBCommentHelper;

import ferit.mihaelaudovicic.osijeksmartguide.R;

import ferit.mihaelaudovicic.osijeksmartguide.model.CommentData;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommViewHolder> {
    private static final String KEY_NAME="username";
    Context context;
    List<CommentData> commentDataList;
    DBCommentHelper DB;


    public CommentsAdapter(Context context, List<CommentData> commentDataList) {
        this.context = context;
        this.commentDataList = commentDataList;
    }

    @Override
    public CommentsAdapter.CommViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.activities_comm_item,parent,false);
        return new CommViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentsAdapter.CommViewHolder holder, int position) {
        CommentData commentData=commentDataList.get(position);
        DB = new DBCommentHelper(context);

        SharedPreferences prefs=context.getSharedPreferences("prefs",Context.MODE_PRIVATE);
        String username = prefs.getString(KEY_NAME,null);

        if(commentData!=null) {
            holder.txt.setText(commentData.getTextComment());

            holder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CommentData cd = commentDataList.get(holder.getAdapterPosition());
                    int sid = cd.getId();
                    String sText = cd.getTextComment();
                    Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.dialog_update);
                    int width = WindowManager.LayoutParams.MATCH_PARENT;
                    int height = WindowManager.LayoutParams.WRAP_CONTENT;
                    dialog.getWindow().setLayout(width, height);
                    dialog.show();

                    EditText editT = dialog.findViewById(R.id.editTxt);
                    Button btnU = dialog.findViewById(R.id.bt_update);
                    editT.setText(sText);

                    btnU.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            String uTxt = editT.getText().toString().trim();
                            DB.updateComm(sid, uTxt, username);

                            commentDataList.clear();
                            commentDataList.addAll(DB.getCommData(username));
                            notifyDataSetChanged();
                        }
                    });
                }
            });

            holder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CommentData cd = commentDataList.get(holder.getAdapterPosition());

                    DB.deleteD(cd);

                    int position = holder.getAdapterPosition();
                    commentDataList.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, commentDataList.size());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return commentDataList.size();
    }

    public class CommViewHolder extends RecyclerView.ViewHolder{
        ImageView btnEdit,btnDelete;
        TextView txt;

        public CommViewHolder(View itemview){
            super(itemview);
            txt=itemview.findViewById(R.id.commView);
            btnEdit=itemview.findViewById(R.id.updBtn);
            btnDelete=itemview.findViewById(R.id.dltBtn);
        }
    }
}
