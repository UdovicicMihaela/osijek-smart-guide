package ferit.mihaelaudovicic.osijeksmartguide.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ferit.mihaelaudovicic.osijeksmartguide.DBFavHelper;
import ferit.mihaelaudovicic.osijeksmartguide.DetailsActivity;
import ferit.mihaelaudovicic.osijeksmartguide.R;
import ferit.mihaelaudovicic.osijeksmartguide.model.FavoritesData;
import ferit.mihaelaudovicic.osijeksmartguide.model.RecentsData;

public class RecentsAdapter extends RecyclerView.Adapter<RecentsAdapter.RecentsViewHolder> {

    Context context;
    List<RecentsData> recentsDataList;
    List<RecentsData> filterRecentsDataList;
    DBFavHelper FavDB;


    private static final String KEY_NAME="username";

    public RecentsAdapter(Context context, List<RecentsData> recentsDataList){
        this.context = context;
        this.recentsDataList = recentsDataList;
        this.filterRecentsDataList=recentsDataList;
    }

    @NonNull
    @Override
    public RecentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        FavDB=new DBFavHelper(context);
        SharedPreferences prefs=context.getSharedPreferences("prefs",Context.MODE_PRIVATE);

        String username = prefs.getString(KEY_NAME,null);
        boolean firstStart=prefs.getBoolean("firstStart",true);

       // if(username != null) {
         //   favBtn.setVisibility(View.VISIBLE);

        if(firstStart)
            createTableOnFristStart();
        View view= LayoutInflater.from(context).inflate(R.layout.recents_row_item, parent,false);
        return new RecentsViewHolder(view);
    }

    private void createTableOnFristStart() {
        FavDB.insertEmpty(); //dodaj u DBFavHelper
        SharedPreferences prefs=context.getSharedPreferences("prefs",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=prefs.edit();
        editor.putBoolean("firstStart",false);
        editor.apply();
    }
    @Override
    public void onBindViewHolder(@NonNull RecentsViewHolder holder, int position) {

       // final RecentsData favData=recentsDataList.get(position);//idk
        //readCursorData(favData,holder);                                      OVO NEĆE

        holder.place_name.setText(filterRecentsDataList.get(position).getPlaceName());
        holder.place_image.setImageResource(filterRecentsDataList.get(position).getImageUrl());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context.getApplicationContext(), "Item Clicked "+recentsDataList.get(position).getPlaceName(),Toast.LENGTH_SHORT).show();
                //Toast.makeText(context.getApplicationContext(), recentsDataList.get(position).getImageUrl(),Toast.LENGTH_SHORT).show();
                Intent i=new Intent(context, DetailsActivity.class);
                i.putExtra("place_id",filterRecentsDataList.get(position).getId());
                i.putExtra("place_name",filterRecentsDataList.get(position).getPlaceName());
                i.putExtra("place_about", filterRecentsDataList.get(position).getAbout());
                i.putExtra("place_image",filterRecentsDataList.get(position).getImageUrl());
                context.startActivity(i);
            }
        });

    }


    @Override
    public int getItemCount() {
        return filterRecentsDataList.size();
    }

    public class RecentsViewHolder extends RecyclerView.ViewHolder{

        SharedPreferences prefs=context.getSharedPreferences("prefs",Context.MODE_PRIVATE);
        String username = prefs.getString(KEY_NAME,null);

        ImageView place_image;
        TextView place_name;
        ImageView favBtn;
        public RecentsViewHolder(@NonNull View itemView) {

            super(itemView);
            place_image=itemView.findViewById(R.id.placeImage);
            place_name=itemView.findViewById(R.id.placeName);
            favBtn=itemView.findViewById(R.id.favoBtn);

            favBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    RecentsData favoritesItem = filterRecentsDataList.get(position); //favdata

                    if (favoritesItem.getFavStatus().equals("0")) {
                        favoritesItem.setFavStatus("1");
                        FavDB.insertData(favoritesItem.getId(), favoritesItem.getPlaceName(), favoritesItem.getAbout(), favoritesItem.getImageUrl(), username,favoritesItem.getFavStatus());//usera???
                        favBtn.setBackgroundResource(R.drawable.ic_baseline_favorite_24);
                    } else {
                        favoritesItem.setFavStatus("0");
                        FavDB.removeFav(favoritesItem.getId());
                        favBtn.setBackgroundResource(R.drawable.ic_baseline_grey_favorite_24);
                    }
                }
            });

        }
    }


    private void readCursorData(RecentsData recentsData, RecyclerView.ViewHolder viewHolder){
        Cursor cursor=FavDB.readAllFavDataOfUser(recentsData.getId());
        SQLiteDatabase db=FavDB.getWritableDatabase();

        try{
            while(cursor.moveToNext()){
                String item_fav_status=cursor.getString(cursor.getColumnIndex(FavDB.FAVORITES_STATUS));
                recentsData.setFavStatus(item_fav_status);
                //provjeri status
                if(item_fav_status!=null && item_fav_status.equals("1")){
                    viewHolder.itemView.findViewById(R.id.favBtn).setBackgroundResource(R.drawable.ic_baseline_favorite_24);

                }else {
                    viewHolder.itemView.findViewById(R.id.favBtn).setBackgroundResource(R.drawable.ic_baseline_grey_favorite_24);
                }
            }
        }finally {
            if (cursor!=null && cursor.isClosed())
                cursor.close();
            db.close();
        }
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String Key = charSequence.toString();
                if (Key.isEmpty()) {
                    filterRecentsDataList = recentsDataList;
                } else {
                    List<RecentsData> lstFiltered = new ArrayList<>();
                    for (RecentsData row : recentsDataList) {
                        if (row.getPlaceName().toLowerCase().contains(Key.toLowerCase())) {
                            lstFiltered.add(row);
                        }
                    }
                    filterRecentsDataList = lstFiltered;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filterRecentsDataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filterRecentsDataList = (List<RecentsData>) filterResults.values;
                notifyDataSetChanged();

            }
        };
    }

}
