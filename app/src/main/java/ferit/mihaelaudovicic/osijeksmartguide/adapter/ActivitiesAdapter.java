package ferit.mihaelaudovicic.osijeksmartguide.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ferit.mihaelaudovicic.osijeksmartguide.ActivitiesDetailsActivity;
import ferit.mihaelaudovicic.osijeksmartguide.R;
import ferit.mihaelaudovicic.osijeksmartguide.model.ActivitiesData;

public class ActivitiesAdapter extends RecyclerView.Adapter<ActivitiesAdapter.ActivitiesViewHolder> {
    Context context;
    List<ActivitiesData> activitiesDataList;

    public ActivitiesAdapter(Context context, List<ActivitiesData> activitiesDataList) {
        this.context = context;
        this.activitiesDataList = activitiesDataList;
    }

    @Override
    public ActivitiesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.activities_row_item, parent,false);
        return new ActivitiesViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ActivitiesAdapter.ActivitiesViewHolder holder, int position) {
        holder.price_name.setText(activitiesDataList.get(position).getPrice());
        holder.place_image.setImageResource(activitiesDataList.get(position).getImageUrl());
        holder.place_name.setText(activitiesDataList.get(position).getPlaceName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(context.getApplicationContext(), "Item Clicked "+activitiesDataList.get(position).getPlaceName(),Toast.LENGTH_SHORT).show();

                Intent i=new Intent(context, ActivitiesDetailsActivity.class);
                i.putExtra("aName", activitiesDataList.get(position).getPlaceName());
                i.putExtra("aPrice",activitiesDataList.get(position).getPrice());
                i.putExtra("aAbout",activitiesDataList.get(position).getAbout());
                i.putExtra("aImage",activitiesDataList.get(position).getImageUrl());
                context.startActivity(i);

            }
        });


    }


    @Override
    public int getItemCount() {
        return activitiesDataList.size();
    }

    public static final class ActivitiesViewHolder extends RecyclerView.ViewHolder{
        ImageView place_image;
        TextView price_name;
        TextView place_name;

        public ActivitiesViewHolder(@NonNull View itemView) {
            super(itemView);
            price_name=itemView.findViewById(R.id.priceName);
            place_name=itemView.findViewById(R.id.activityName);
            place_image=itemView.findViewById(R.id.activityImage);
        }
    }
}
