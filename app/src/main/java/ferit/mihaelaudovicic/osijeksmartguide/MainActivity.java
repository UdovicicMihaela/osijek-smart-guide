package ferit.mihaelaudovicic.osijeksmartguide;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import ferit.mihaelaudovicic.osijeksmartguide.adapter.ActivitiesAdapter;
import ferit.mihaelaudovicic.osijeksmartguide.adapter.RecentsAdapter;
import ferit.mihaelaudovicic.osijeksmartguide.model.ActivitiesData;
import ferit.mihaelaudovicic.osijeksmartguide.model.RecentsData;

public class MainActivity extends AppCompatActivity {

    RecyclerView recentRecycler, activityRecycler;
    RecentsAdapter recentsAdapter;
    ActivitiesAdapter activitiesAdapter;
    EditText searchView;
    CharSequence search="";
    ImageView user;
    ImageView fav;
    SharedPreferences sharedPreferences;

    private static final String SHARED_PREF_NAME="mypref";
    private static final String KEY_NAME="username";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchView=findViewById(R.id.editTextTextPersonName);
        user=findViewById(R.id.userView);

        sharedPreferences=getSharedPreferences(SHARED_PREF_NAME,MODE_PRIVATE);
        String username=sharedPreferences.getString(KEY_NAME,null);

        List<RecentsData> recentsDataList = new ArrayList<>();

        recentsDataList.add(new RecentsData( "1","Rodina gnijezda u centru grada","Osijek je grad okružen dravskim poplavnim šumama i oranicama, a gotovo na rubu grada započinje i područje parka prirode Kopački rit. Tako nije neobično u blizini samog glavnog trga vidjeti gnijezda bijelih roda. Trenutno su aktivna dva, na dvorišnoj strani bivšeg hotela Royal i na vrhu tornjića zgrade „Ipk“ na početku korza. Do nedavno je aktivno bilo i gnijezdo na samoj Katedrali, a graciozne velike bijele ptice bile su zanimljiv kontrast strašnim gargolijama, vodorigama. U Zimskoj luci u Gornjem gradu svakoga se jutra može vidjeti mnoštvo divljih ptica na jutarnjem hranjenju, a nije neobično niti da u grad zaluta pokoja srna. Divlje životinje kojih vjerojatno ima najviše u gradu su kune, čistačice grada, a najaktivnije su u noćnim satima.",
                R.drawable.rodinagnijezda,"0"));
        recentsDataList.add(new RecentsData( "2","Dvorac Pejačević", "Poznata hrvatska plemićka obitelj, grofovi Pejačević imali su posjede po čitavoj Hrvatskoj. Iz ove je obitelji potekla i prva hrvatska skladateljica Dora Pejačević. Muški članovi obitelji obnašali su dužnost hrvatskog bana, danas bismo rekli premijera, a njihov politički i gospodarski utjecaj širio se diljem nekadašnjeg Austro-ugarskog carstva. Nakon 1945. godine, članovi ove obitelji iz jedine preživjele „našičke“ grane bili su prisiljeni napustiti Hrvatsku pod pritiskom komunističkih vlasti. Na području Slavonije posjedovali su tri velika dvorca, među kojima i ovaj u osječkoj gradskoj četvrti Retfala. Dvorac je građen na prijelazu iz 18. u 19. stoljeće u barokno-klasicističkom stilu. Od početka 20. stoljeća dvorac je u vlasništvu Hrvatske provincije Marijinih sestara čudotvorne medaljice te se u većem dijelu dvorca nalazi njihov samostan, dok se u njegovu zapadnom krilu nalazi privatna bolnička klinika. Zanimljivi i živopisni opisi dvorca i njegovih stanovnika mogu se pronaći u popularnom povijesno-ljubavnom romanu Marije Jurić Zagorke „Vitez slavonske ravni“.",R.drawable.dvoracpejacevic,"0"));
        recentsDataList.add(new RecentsData( "3","Mačkamama","Vjerojatno najpopularnija osječanka bila je i do danas ostala barunica Paulina Hermann, velika dobrotvorka. Kažu da nitko u potrebi od nje nije otišao praznih ruku, no ipak je najpoznatija ostala po velikoj ljubavi prema životinjama, posebice mačkama. Kada je 1925. godine u gradskoj četvrti Industrijska četvrt izgradila novu vilu gradom se odmah pročulo kako s njom živi veliki broj mačaka i pasa, a pričalo se da posjeduje čak i lava. Upravo zato osječani su ju prozvali Mačkamamom. U sjećanje na poznatu sugrađanku ispred „dvorca“ postavljen je hrastov kip barunice okružene mačkama, djelo osječkog umjetnika Mate Tijardovića.", R.drawable.mackamama,"0"));
        recentsDataList.add(new RecentsData("4","Spomenik Crvenom Fići", "Građani Hrvatske su 1990. na referendumu odlučili iskoristiti pravo koje je pružao Ustav komunističke Socijalističke Federativne Republike Jugoslavije te je odlučeno da Hrvatska nakon 45 godina ponovno postane samostalna država s demokratskim uređenjem i slobodnim tržištem. Vodstvo Jugoslavenske Narodne Armije, pod utjecajem rukovoditelja Komunističke partije Srbije vođenima iskrivljenom povijesnom idejom stvaranja tzv. \"Velike Srbije\", nije prihvatilo rezultat referenduma te su 1991. godine uz pomoć srpskih paravojnih jedinica započeli rat protiv Hrvatske. Prije zahuktavanja rata Armija je odlučila pokazati svoju snagu (radilo se o jednoj od najvećih europskih vojnih sila) izlaskom tenkova na ulice Osijeka. Dana 27. lipnja 1991. za vrijeme divljanja tenkova gradskim ulicama sugrađanin Branko Breškić pokušao ih je zaustaviti parkirajući posred raskrižja Trpimirove i Vukovarske ulice svoj auto, popularni jugoslavenski model Fiat 500 zvan „Fićo“. Tenk ga je smrskao u komadiće oštetivši tako obližnji autobus. U sjećanje na taj događaj, jasne najave ratnih strahota koje su potrajale i tijekom 1992. godine, na raskrižju je postavljena umjetnička instalacija u kojoj mali „hrabri“ crveni Fićo gazi tenk te tako simbolizira pobjednika u nametnutom ratu. Hrvatska je unatoč ratnim žrtvama i posrnulom gospodarstvu od 1992. godine međunarodno priznata članica UN-a, a 2013. primljena je u članstvo Europske Unije.",R.drawable.crvenfici,"0"));
        recentsDataList.add(new RecentsData("5","Trg sv. Trojstva", "Glavnim trgom u starom gradu, Tvrđi, dominira barokni kip Sv. Trojstva. Izgrađen je 1729. godine, kao zavjetni kip Osječana dragom Bogu da se kuga koja je prethodno poharala grad više nikada ne vrati. Legenda pak kaže da su gradske štakore pa time i kugu istrijebile simpatične životinjice koje su osječani u velikom broju naselili u grad – kune (zanimljivo je kako su Osječani i u kasnijim stoljećima u borbi protiv nametnika posezali za \"eko-rješenjima\" pa je u novinama s početka 20. stoljeća zapisano da su u poplavne šume oko grada, posebice u Baranji, naselili šišmiše kako bi pomogli smanjiti broj dosadnih komaraca)! Po mnogima kip Sv. Trojstva u Osijeku je najljepši barokni spomenik u Hrvatskoj. Slični zavjetni kipovi postoje u mnogim gradovima pa i selima u Slavoniji i Baranji te diljem nekadašnjeg Austro-ugarskog carstva. Na istočnom uglu trga stoji zgrada Glavne gradske straže s tornjem-stražarnicom izgrađena početkom 18.stoljeća, a u njoj se danas nalazi Arheološki odjel Muzeja Slavonije. Dijagonalno na uglu nalazi se Muzej Slavonije. Ispred njega raste prastaro stablo ginka, ostatak nekadašnjeg velikog parka. Na suprotnoj zapadnoj strani trga na uglu nalazi se Glazbena škola Franje Kuhača. Rođeni Osječanin, Kuhač je u drugoj polovini 19. stoljeća postao poznat kao sakupljač slavenske glazbene narodne baštine. Svojim istraživanjem dokazao je kako su poznati svjetski skladatelji Ludwig van Beethoven i Joseph Haydn, po majci Hrvat, u svojim djelima često posuđivali dijelove hrvatske tradicijske glazbe. Zanimljivo je kako Haydnova melodija današnje njemačke himne potječe od stare hrvatske pjesme \"Stal se jesem rano jutro malo pred zoru\" iz Međimurja i austrijskog Gradišća, iako ju je Haydn \"posudio\" kako bi skladao carsku himnu Bože živi, Bože štiti (Gott erhalte, Gott beschütze).", R.drawable.trgsvtrojstva,"0"));
        recentsDataList.add(new RecentsData("6","Viseći most i secesijski zdenac", "Vjerojatno najčešći motiv na razglednicama Osijeka je sljedeći: pogled na grad s lijeve dravske obale s visećim pješačkim mostom te zvonikom katedrale i staklenim tornjem hotela uz pozadini. Most Mladosti izgrađen je 1980. godine, a danas je omiljen među zaljubljenim parovima koji na njegovu ogradu stavljaju tzv. ljubavne lokote. Vjeruje se da će njihova ljubav biti sigurna sve dok je ključ lokota duboko na dnu rijeke Drave. Na dijelu dravske promenade između visećeg mosta i Zimske luke nalazi još jedan simbol Osijeka, veliki secesijski zdenac, poklon grofovske obitelji Pejačević gradu Osijeku iz 1903. godine.",R.drawable.visecimostiseceszedn,"0"));
        recentsDataList.add(new RecentsData("7", "Europska avenija","Po secesiji, stilu vrlo popularnom u cijelom nekadašnjem Austro-ugraskom carstvu, Osijek je posebno poznat. Veličanstvene gradske palače u Europskoj aveniji, danas većinom raskošni uredski prostori, građene su na samom početku 20. stoljeća u stilu tzv. „bečke secesije“ dok je najveća među njima, Poštanska palača, izgrađena u stilu „mađarske secesije“. U istoj ulici nalazi se i niz klasicističkih zgrada iz 19. stoljeća s monumentalnom Sudbenom palačom. Od svih kuća u ovoj ulici vjerojatno je najposjećenija ona Muzeja likovnih umjetnosti. U njenom se fundusu nalazi cijelo bogatstvo umjetničkih slika i skulptura. Neke od najzanimljivijih slika su portreti slavonskih plemićkih obitelji iz 18. i 19. stoljeća te romantični pejzaži Slavonije i Baranje djela utemeljitelja osječke crtačke škole Huga Conrada Von Hötzendorfa i Adolfa Waldingera.", R.drawable.europskaavenija,"0"));
        recentsDataList.add(new RecentsData("8","Kon-katedrala sv. Petra i pavla (osječka katedrala)" , "Prema inicijativi biskupa Josipa Jurja Strossmayera, 1894. godine započela je izgradnja crkve Sv. Petra i Pavla, kon-katedrale Đakovačko-osječke nadbiskupije. Upravo zato ju Osječani nazivaju jednostavno „katedralom“. Nakon rušenja starije barokne crkve, dovršena je u samo četiri godine te je od 1898. godine panorama Osijeka ne zamisliva bez njezinog 90 metara visokog zvonika i danas drugog po visini u Hrvatskoj i jugoistočnoj Europi. Građena je od crvene fasadne opeke, u neo-gotičkom stilu, a graditelji su bili njemački arhitekti Franz Langenberg i Richard Jordan. Oslikavanje zidova freskama izveo je poznati hrvatski slikar Mirko Rački. Crkva ima pet zvona. Najveće ujedno i treće po veličini u Hrvatskoj mase više od 2,5 tone posvećeno Sv. Petru i Pavlu nalazi se u glavnom tornju iznad sata. U ratu 1991. i napadima jugoslavenske vojske „katedrala“ je pogođena više od stotinu puta te je u tijeku cjelovita obnova. Zanimljivo je da se u njemačkom gradu Kölnu nalazi nešto novija crkva Srca Isusova koja je gotovo kopija osječke katedrale.", R.drawable.webkatedrala,"0"));
        recentsDataList.add(new RecentsData("9", "Eurodom i žalosna kapelica","Južno od osječkog starog grada Tvrđe nalazi se trgovačko-kulturni centar Eurodom. Ovaj stakleni neboder kojega Osječani nazivaju i „tornjevima blizancima“ visok je 61 metar što ga nakon Hotela Osijek (62 metra) i Katedrale (90 metara) čini trećom najvišom zgradom u gradu i srednjedunavskoj regiji. Zgrada se nalazi na mjestu starijeg Radničkog doma, monumentalne multifunkcionalne zgrade s počekta 40-ih godina 20. stoljeća za koju se sumnjalo da je građena u svrhu jačanja osjećaja pripadnosti Trećem Reichu u vrijeme okupacije grada u Drugom svjetskom ratu. Zanimljivost je veća kad se uzme u obzir da je temeljni kamen za nju položen 14. travnja 1942., a grad je oslobođen na isti datum 1945. godine. Pored Eurodoma nalazi se „Žalosna kapelica“, omanja barokna građevina iz 1780. godine, poznata po legendi o „osječkom Romeu i Juliji“. Početkom 18. stoljeća ovdje je naime na smrt osuđen mladić zbog krađe koju nije počinio što je prešutio ne bi li zaštitio ugled svoje djevojke.",R.drawable.zalosnakapela,"0"));
        recentsDataList.add(new RecentsData("10", "Kino Urania i Kino Europa","U srcu Gornjeg grada razdvojene romantičnim Sakuntala parkom nalaze se dvije stare kino dvorane. Kino Urania iz 1912. i Kino Europa iz 1939. godine. Kino Europa koje se danas koristi za povrmene projekcije te razna kulturna događanja mnogi smatraju jednim od najljepših djela moderne arhitekture u Osijeku. Kino Urania, djelo Osječanina Viktora Axmana, je prema stručnjacima jedno od najboljih ostvarenja secesijske arhitekture u gradu pa i šire. Kino je navodno građeno ne samo za potrebe filmskih predstava već i kako bi se u nju smjestila masonska loža „Budnost“. Sama zgrada puna je simbolike vidljive i na pročelju, od piramidalnog oblika do reljefa sfinge, čuvarice „hrama“. Iako osječka masonska loža više nije aktivna, zanimljiva je legenda prema kojoj su svoj \"hram\" namjerno postavili pročeljem nasuprot Osječkoj katedrali, ne bi li pokazali svoju moć i društveni utjecaj ravan crkvenomu...", R.drawable.websakuntalapark,"0"));
        recentsDataList.add(new RecentsData("11","Gradske zidine i vodena vrata","Početkom 18. stoljeća započela je izgradnja zidina osječke tvrđave – Tvrđe, u obliku velike nepravilne zvijezde. Po dovršetku radova 1722. godine, Tvrđa je postala jedna od najvećih i najmodernijih vojnih utvrda u srednjoj Europi. Unutar zidina zgrade su građene gotovo isključivo u baroknom stilu, a u sam grad vodilo je četvora vrata. U prvoj je polovini 20. stoljeća na žalost odlučeno da se stare zidine sruše. Tako danas postoji tek manji dio zidina prema rijeci s Vodenim vratima i kulom (vodotoranj), te veći dio Krunske utvrde na lijevoj obali Drave. Osječke zidine građene su ponajprije zbog obrane od mogućeg novog napada vojske Osmanskog carstva koje je Osijek držalo pod okupacijom od 1526. do 1687. godine. Gradnju zidina vodio je general Johann Stephan von Beckers, a i danas se vjeruje da je njegovo tijelo po smrti zazidano u zidine uz Vodena vrata. Zanimljiva je anegdota o pokušaju ulaska cara Josipa Drugog u Tvrđu. Naime, tijekom svog prvog posjeta gradu 1768., car je došao nakon zalaska sunca te se čak niti njemu nisu smjela otvoriti gradska vrata. S toga su mu smještaj ponudili u Gornjem gradu u gostionici K šaranu na mjestu današnjeg Esseker centra. Još jedna povijesna legenda kaže da je u vrijeme Napoleonskih ratova, krajem 1805. godine kad je veliki vojskovođa zaprijetio glavnom gradu Habsburškog carstva, cijela carska riznica preseljena među zidine osječke Tvrđe, koja je slovila za najsigurniju utvrdu u čitavoj zemlji. Po prolasku opasnosti riznica je vraćena u Beč no jedna je škrinja s državnim blagom navodno ostala u Osijeku te do danas nije pronađena…",R.drawable.vodenavrata,"0"));
        recentsDataList.add(new RecentsData("12","Utvrda Korogyvar","Korogyvar (Korođgrad) srednjovjekovna tvrđava (wasserburg) iz 13. stoljeća, okružena većinom isušenom močvarom, nalazi se nadomak Osijeku između sela Ivanovac i Čepin. Do nje se može lako doći biciklističkom stazom iz osječke Industrijske četvrti ili automobilom. Ova ciglom i kamenom građena kružna utvrda dugo je bila vlasništvo srednjovjekovnih osječkih vladara, obitelji Korogy, a od 16. stoljeća i ratovima s Osmanskim carstvom stoji u ruševinama. No unatoč ruševnom stanju i dalje je među lokalnim stanovništvom omiljeno izletište, a priče i legende o skrivenom blagu baruna Trenka, o njegovoj čuvarici zmijskoj djevojci i krikovima u noći koji lede krv u žilama i danas se sa strahopoštovanjem prepričavaju. U jesen 2014. godine ovdje je održan srednjovjekovni sajam te viteški turnir koji će zasigurno prijeći u zanimljivu tradiciju.",R.drawable.korogyvar,"0"));

        setRecyclerView(recentsDataList);

        List<ActivitiesData> activitiesDataList=new ArrayList<>();
        activitiesDataList.add(new ActivitiesData("Kompa","Besplatno",R.drawable.kompa,"Kada ste u Osijeku, Dravu možete prijeći mostom poput elegentnog visećeg „Mosta Mladosti“ ili „kompom“, skelom. Kompa je u Osijeku počela prometovati 1916. godine, a ne provozati se njom s jedne na drugu obalu rijeke bilo bi ravno posjetu Zagrebu i ne provozati se uspinjačom. Uz to, Osječanima je kompa omiljeno prijevozno sredstvo za posjet zoološkom vrtu i lijepom parku koji ga okružuje. Drugi razlog korištenja skele-kompe je rekreacija. Krug biciklom, na rolama ili laganom šetnjom po promenadi uz Dravu ne zamisliv je bez korištenja ovog zanimljivog i ekološkog plovila. Kompu naime pokreće jedino struja rijeke Drave, a kako ne bi otplovila nizvodno prema obližnjem Dunavu „čvrsto se drži“ za čelično uže dopremljeno iz rudnika s dalekih Karpata. Vozni red: polazak s obje obale u intervalima, svakih 20 minuta."));
        activitiesDataList.add(new ActivitiesData("Zoološki vrt","20 kuna",R.drawable.zoo,"Osječki zoološki vrt osnovan je 1955. godine. Zahvaljujući lokaciji udaljenoj od gradske buke, savršeno je mjesto za mirni boravak 80-ak vrsta životinja u kavezima i u otvorenim nastambama. U akvariju i terariju živi još 20-ak vrsta riba i gmazova, a ukupno u parku živi 650 jedinki životinja. Uz površinu od 11 hektara, osječki zoo jedan je od većih parkova u srednjem podunavlju, a velika vrijednost parka je upravo u njegovoj lokaciji. Ukoliko vam se nakon jednog posjeta učini da ste sve već vidjeli, sigurno ćete poželjeti posjetiti ga ponovo i jednostavno uživati u miru i tišini gotovo nedirnute prirodne podravske šume. Do Zoo-vrta nalazi se hotel i ugostiteljski objekt te dječje igralište. Uz pogled na rijeku Dravu i okolno zelenilo ovo je zasigurno jedno od najljepših mjesta za opuštanje i „punjenje baterija“ u gradu."));
        activitiesDataList.add(new ActivitiesData("Copacabana","Besplatno",R.drawable.copacabana,"Svi koji su tijekom održavanja Svjetskog nogometnog prvenstva 2014. godine s tugom prihvatili činjenicu da su Brazil, Rio i tamošnja popularna plaža Copacabana nedostižno daleko, ako su posjetili Osijek mogli su odahnuti. Naime najveće i najposjećenije dravsko kupalište izgrađeno 1965. nosi naziv upravo po toj poznatoj plaži, od milja u skraćenoj domaćoj varijanti zvanoj „Kopika“. Ondje se tijekom ljetnih mjeseci možete osvježiti kupanjem u Dravi, poprimiti brončani ten na pješčanoj plaži ili se zabaviti na obližnjim otvorenim bazenima. Uz pogled na cijeli grad i glavne gradske znamenitosti te cijenu ulaznice koja je gotovo simbolična, osječka Copacabana je ljetno odredište br. 1! U blizini kupališta nalazi se i neslužbeno no ipak omiljeno izletište „kod katakombi“, smješteno u Krunskoj utvrdi – sjevernom dijelu osječke tvrđave. Ovdje je zasigurno najveselije tijekom mjeseca rujna u vrijeme održavanja omiljenih Dana prvog hrvatskog piva.\n" +
                "\n"));
        activitiesDataList.add(new ActivitiesData("Mlin - vodenica na Dravi","Besplatno",R.drawable.vodenica,"U blizini skele „Kompe“, stare vodovodne centrale i vidikovca nad Dravom u jesen 2015. godine porinuto je vrlo zanimljivo plovilo – replika vodenice, mlin-brod. Od 18. do početka 20. stoljeća godišnje ih je u Osijeku bilo i preko 60. Do sredine 20. stoljeća na velikim slavonskim rijekama Dravi, Savi i Dunavu radilo je više stotina mlinova, a zadnja je vodenica u Osijeku prestala s radom 1944. godine. Mlinarski je obrt bio jedan od najcjenjenijih. U želji da se oživi sjećanje na slavno doba mlinarstva tijekom kojega je Osijek postao središte proizvodnje žitarica i pekarskih proizvoda (ovdje djeluju neki od najvećih hrvatskih i regionalnih proizvođača – uzgoj žitarica: Žito d.d., lanac pekarnica: Mlinar d.d., tvornica keksa: Karolina d.o.o.), pokrenut je projekt pod nazivom „Mlinarev put“. "));
        activitiesDataList.add(new ActivitiesData("Tramvaj","11 kuna",R.drawable.tramvaj,"Dana 10. rujna 1884. u Osijeku je počeo prometovati konjski tramvaj, jedan od prvih u srednjoj i jugoistočnoj Europi. Od većih gradova u regiji jedino su Beč, Budimpešta, Graz i Bukurešt tramvaj dobili prije Osijeka dok su ga gradovi Zagreb, Bratislava, Sofia i Beograd uveli kasnije. Unatoč velikim željama da prestignu Osječane, u Sarajevu je tramvaj zakasnio tri mjeseca. Mnogi gradovi svijeta danas ponovo uvode tramvaje u sustav javnog prijevoza, a Osijek je među rijetkim \"malim\" gradovima koji se mogu pohvaliti njegovim neprekinutim postojanjem i proširenjem. Na osječkim se ulicama trenutno može vidjeti četiri različita modela tramvaja od kojih je najstariji onaj iz 1926. godine te služi za turističke vožnje gradom. Zanimljivo je kako je prvi tramvaj iz 1884. bio crvene boje, navodno po želji cara Franje Josipa kako bi sličio bečkim tramvajima. Danas su tramvaji bijelo - plavi, u gradskim bojama."));

        setActivityView(activitiesDataList);

        Button btn= findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aboutInt=new Intent(getApplicationContext(), AboutActivity.class);
                startActivity(aboutInt);
            }
        });

        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(username != null){
                    Intent LogoutInt = new Intent(getApplicationContext(), UserDetails.class);
                    startActivity(LogoutInt);

                }else {
                    Intent logInt = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(logInt);
                }
            }
        });

        /*
        if(username!=null){
        fav=findViewById(R.id.favoBtn);
            if(recentsDataList.size()>1) {
                fav.setVisibility(View.VISIBLE); //error

                fav.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fav.setImageResource(R.drawable.ic_baseline_favorite_24);


                    }
                });
            }
        }*/


        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                recentsAdapter.getFilter().filter(s);
                search=s;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void setRecyclerView(List<RecentsData> recentsDataList){
        recentRecycler=findViewById(R.id.recents_recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        recentRecycler.setLayoutManager(layoutManager);
        recentsAdapter= new RecentsAdapter(this, recentsDataList);
        recentRecycler.setAdapter(recentsAdapter);
       /* ImageView favbtn=findViewById(R.id.favoBtn);
        if(username!=null){
            favbtn.setVisibility(View.VISIBLE);
        }*/
    }

    private void setActivityView(List<ActivitiesData> activitiesDataList){
        activityRecycler=findViewById(R.id.activities);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        activityRecycler.setLayoutManager(layoutManager);
        activitiesAdapter= new ActivitiesAdapter(this, activitiesDataList);
        activityRecycler.setAdapter(activitiesAdapter);
    }

}