package ferit.mihaelaudovicic.osijeksmartguide;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.Nullable;

public class DBFavHelper extends SQLiteOpenHelper {
    public static final String DBNAME="myfavdb.db";
    SharedPreferences prefs;
    Context context;

    private static final String SHARED_PREF_NAME="mypref";
    private static final String KEY_NAME="username";
    public static String FAVORITES_STATUS="fStatus";
    public static String KEY_ID="id"; //?
    public static String ITEM_TITLE="name"; //?
    public static String ITEM_ABOUT="about"; //?
    public static String ITEM_IMAGE="image";


    public DBFavHelper( Context context) {
        super(context, DBNAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table fav(id TEXT, name TEXT, about TEXT, image TEXT, username TEXT, fStatus TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists fav");
    }

    public  void insertEmpty(){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        for(int i=1;i<11;i++){
            cv.put(KEY_ID,i);
            cv.put(FAVORITES_STATUS,"0");
            db.insert("fav",null,cv);
        }
    }

    public boolean insertData(String id, String name, String about, Integer image, String userN,String fStatus){

        //sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        //String username=sharedPreferences.getString(KEY_NAME,null);

        SQLiteDatabase MyDB=this.getWritableDatabase();

        ContentValues contentValues=new ContentValues();
        contentValues.put(KEY_ID,id);
        contentValues.put(ITEM_TITLE,name);
        contentValues.put(ITEM_ABOUT,about);
        contentValues.put(ITEM_IMAGE,image);
        contentValues.put(KEY_NAME,userN);
        contentValues.put(FAVORITES_STATUS,fStatus);

        long result=MyDB.insert("fav", null, contentValues);
        if(result==-1){
            return false;
        }else{
            return true;
        }
    }

    public void removeFav(String id){
        SQLiteDatabase MyDB=this.getWritableDatabase();
        String sql="update fav set fStatus = '0' and "+KEY_ID+" = "+id+"";
       // MyDB.delete("fav","name=?",new String[]{name});
       // return MyDB.rawQuery(sql,null,null);
        MyDB.execSQL(sql);
        Log.d("remove favorite item","id.toString()");
    }

    public boolean checkUser(String userName){
        SQLiteDatabase MyDB=this.getWritableDatabase();

        Cursor cursor=MyDB.rawQuery("select * from users where username=?",new String[]{userName});

        if(cursor.getCount()>0){
            return true;
        }else{
            return false;
        }
    }

    public Cursor readAllFavDataOfUser(String id){
        SQLiteDatabase MyDB=this.getWritableDatabase();
        String sql = "select * from fav where "+KEY_ID+" = " + id+"";
        return MyDB.rawQuery(sql, null, null);
    }
    public Cursor selectAllFavoriteList(){
        SQLiteDatabase db=this.getWritableDatabase();
        String sql="select * from favs where fStatus = '1'";
        return db.rawQuery(sql,null,null);
    }

}
