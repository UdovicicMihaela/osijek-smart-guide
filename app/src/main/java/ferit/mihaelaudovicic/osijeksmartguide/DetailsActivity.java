package ferit.mihaelaudovicic.osijeksmartguide;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import ferit.mihaelaudovicic.osijeksmartguide.model.RecentsData;

public class DetailsActivity extends AppCompatActivity {

    ImageView aImage;
    TextView aName;
    TextView aAbout;
    String name,about,id; //id
    Integer image;
    ImageView back;
    ImageView fav;
    SharedPreferences sharedPreferences;

    private static final String SHARED_PREF_NAME="mypref";
    private static final String KEY_NAME="username";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        sharedPreferences=getSharedPreferences(SHARED_PREF_NAME,MODE_PRIVATE);
        String username=sharedPreferences.getString(KEY_NAME,null);

        fav=findViewById(R.id.favView);
        back = findViewById(R.id.backView);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
            }
        });

        aImage = findViewById(R.id.aImage);
        aName = findViewById(R.id.aName);
        aAbout = findViewById(R.id.aAbout);

        name=getIntent().getStringExtra("place_name");
        image=getIntent().getIntExtra("place_image",1);

        id=getIntent().getStringExtra("place_id");
        about=getIntent().getStringExtra("place_about");

        Glide.with(this).load(image).into(aImage);

        aName.setText(name);
        aAbout.setText(about);


       /* if(username!=null){
            fav.setVisibility(View.VISIBLE);

            fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fav.setImageResource(R.drawable.ic_baseline_favorite_24);

                    Intent add=new Intent(getApplicationContext(),UserDetails.class);
                    add.putExtra("namee",name);
                    add.putExtra("imagee", image);
                    add.putExtra("aboutt",about);
                    add.putExtra("idd",id);

                }
            });
        }*/

    }


}