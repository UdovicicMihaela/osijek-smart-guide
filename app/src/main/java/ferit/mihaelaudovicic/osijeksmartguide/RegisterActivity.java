package ferit.mihaelaudovicic.osijeksmartguide;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class RegisterActivity extends AppCompatActivity {

    ImageView back;
    Button reg;
    EditText userReg;
    EditText passReg;
    EditText repassReg;
    DBHelper DB;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        userReg=findViewById(R.id.usernameReg);
        passReg=findViewById(R.id.passReg);
        reg=findViewById(R.id.buttonReg);
        back=findViewById(R.id.back3View);
        repassReg=findViewById(R.id.repassView);
        DB=new DBHelper(this);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
            }
        });

        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = userReg.getText().toString();
                String pass = passReg.getText().toString();
                String repass = repassReg.getText().toString();
                if (user.equals("") || pass.equals("")) {
                    Toast.makeText(RegisterActivity.this, "Unesite podatke", Toast.LENGTH_SHORT).show();
                } else {
                    if (pass.equals(repass)) {
                        Boolean checkuser = DB.checkUsername(user);
                        if (checkuser == false) {
                            Boolean insert = DB.insertData(user, pass);
                            if (insert == true) {
                                Toast.makeText(RegisterActivity.this, "Registrirani ste", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(RegisterActivity.this,"Registration failed",Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(RegisterActivity.this,"Korisnik već postoji",Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(RegisterActivity.this,"Lozinke nisu iste!",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }
}
