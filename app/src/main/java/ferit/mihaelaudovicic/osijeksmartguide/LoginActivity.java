package ferit.mihaelaudovicic.osijeksmartguide;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    ImageView back;
    Button loginbtn;
    Button regbtn;
    EditText userLog;
    EditText passLog;
    DBHelper DB;
    SharedPreferences sharedPreferences;

    private static final String SHARED_PREF_NAME="mypref";
    private static final String KEY_NAME="username";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        userLog=findViewById(R.id.usernameLog);
        passLog=findViewById(R.id.passLog);

        loginbtn=findViewById(R.id.buttonLogin);
        regbtn=findViewById(R.id.buttonRegister);
        DB=new DBHelper(this);
        sharedPreferences=getSharedPreferences(SHARED_PREF_NAME,MODE_PRIVATE);

        back=findViewById(R.id.back2View);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
            }
        });

        regbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ir=new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(ir);

            }
        });

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user=userLog.getText().toString();
                String pass=passLog.getText().toString();


                if (user.equals("")||pass.equals("")){
                    Toast.makeText(LoginActivity.this,"Unesite podatke",Toast.LENGTH_SHORT);
                }else{
                    Boolean checkuser=DB.checkUsernamePassword(user,pass);
                    if(checkuser==true){
                        SharedPreferences.Editor editor=sharedPreferences.edit();
                        editor.putString(KEY_NAME,user);
                        editor.apply();

                        Toast.makeText(LoginActivity.this,"Prijavljeni ste",Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(LoginActivity.this,"Krivo ste unijeli korisničko ime ili lozinku",Toast.LENGTH_SHORT).show();

                    }
                }

            }
        });
    }
}
