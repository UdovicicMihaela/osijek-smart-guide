package ferit.mihaelaudovicic.osijeksmartguide;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

public class ActivitiesDetailsActivity extends AppCompatActivity {

    ImageView back;

    ImageView image;
    TextView name;
    TextView about;
    TextView price;
    String actname,actabout,actprice;
    Integer actimage;

    @Override
    public void onCreate(@Nullable Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_activities_details);

        back = findViewById(R.id.backkView);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
            }
        });

        image=findViewById(R.id.imageView);
        name=findViewById(R.id.nameView);
        about=findViewById(R.id.aboutView);
        price=findViewById(R.id.priceView);

        actname=getIntent().getStringExtra("aName");
        actimage=getIntent().getIntExtra("aImage",1);
        actprice=getIntent().getStringExtra("aPrice");
        actabout=getIntent().getStringExtra("aAbout");

        name.setText(actname);
        about.setText(actabout);
        price.setText(actprice);
        Glide.with(this).load(actimage).into(image);

    }

}
