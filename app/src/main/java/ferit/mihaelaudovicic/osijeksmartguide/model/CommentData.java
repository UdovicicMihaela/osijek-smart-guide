package ferit.mihaelaudovicic.osijeksmartguide.model;

import java.io.Serializable;

public class CommentData implements Serializable {
    Integer id;
    String textComment;

    public CommentData() {
    }

    public CommentData(Integer id, String textComment) {
        this.id=id;
        this.textComment = textComment;
    }

    public String getTextComment() {
        return textComment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTextComment(String textComment) {
        this.textComment = textComment;
    }
}
