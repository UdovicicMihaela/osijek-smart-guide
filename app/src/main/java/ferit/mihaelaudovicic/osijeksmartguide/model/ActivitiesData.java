package ferit.mihaelaudovicic.osijeksmartguide.model;

public class ActivitiesData {
    String placeName;
    String price;
    Integer imageUrl;
    String about;

    public ActivitiesData(String placeName, String price, Integer imageUrl,String about) {
        this.placeName = placeName;
        this.price = price;
        this.imageUrl = imageUrl;
        this.about=about;
    }

    public String getPlaceName() {
        return placeName;
    }

    public String getPrice() {
        return price;
    }

    public Integer getImageUrl() {
        return imageUrl;
    }

    public String getAbout() {
        return about;
    }
}
