package ferit.mihaelaudovicic.osijeksmartguide.model;

public class RecentsData {
    String id;
    String placeName;
    String about;
    Integer imageUrl;
    String favStatus;

    public RecentsData(String id, String placeName, String about, Integer imageUrl,String favStatus) {
        this.id = id;
        this.placeName = placeName;
        this.about = about;
        this.imageUrl = imageUrl;
        this.favStatus=favStatus;
    }

    public String getId() {
        return id;
    }

    public String getPlaceName() {
        return placeName;
    }

    public String getAbout() {
        return about; }

    public Integer getImageUrl() {
        return imageUrl;
    }

    public String getFavStatus() {
        return favStatus;
    }

    public void setFavStatus(String favStatus) {
        this.favStatus = favStatus;
    }
}
