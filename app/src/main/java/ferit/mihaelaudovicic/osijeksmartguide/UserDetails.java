package ferit.mihaelaudovicic.osijeksmartguide;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ferit.mihaelaudovicic.osijeksmartguide.adapter.CommentsAdapter;
import ferit.mihaelaudovicic.osijeksmartguide.adapter.FavoriteAdapter;
import ferit.mihaelaudovicic.osijeksmartguide.model.CommentData;
import ferit.mihaelaudovicic.osijeksmartguide.model.FavoritesData;


public class UserDetails extends AppCompatActivity {

    RecyclerView favs;
    FavoriteAdapter favAdapter;
    Button lobtn;
    TextView usernamee;
    SharedPreferences sharedPreferences;
    ImageView backToH;
    DBFavHelper DB;
    List<FavoritesData> favsList =new ArrayList<>();

    EditText comm;
    Button addBtn;
    DBCommentHelper DBcom;
    RecyclerView commView;
    List<CommentData> commList=new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    CommentsAdapter adapter;

    private static final String SHARED_PREF_NAME="mypref";
    private static final String KEY_NAME="username";

    String name,about;
    Integer image,id;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userdetails);

        backToH = findViewById(R.id.backToHome);

        comm=findViewById(R.id.enterComment);
        addBtn=findViewById(R.id.addBtn);

        DBcom=new DBCommentHelper(this);
        commView=findViewById(R.id.comments_recycle_view);

        usernamee = findViewById(R.id.usernametextView);
        lobtn = findViewById(R.id.LogoutBtn);
        sharedPreferences = getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
        String username = sharedPreferences.getString(KEY_NAME, null);

        name = getIntent().getStringExtra("namee");
        about = getIntent().getStringExtra("aboutt");
        image = getIntent().getIntExtra("imagee", 1);
        //id = getIntent().getIntExtra("idd", -1);


        favs = findViewById(R.id.favoriti_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        favs.setHasFixedSize(true); //idk
        favs.setLayoutManager(layoutManager);

        DB = new DBFavHelper(this);
        //loadData();           OVO NEcE

        // if(usernamee!=null){
        //   loadData();

            /*
            Boolean insert = DB.insertData(name,about,image,username,"1");
            if(insert){
                favss.add(new FavoritesData(id,name,about,image,"1")); //?
                setFavsView(favss);
            }else{
                Log.e("UserDetails","Greska kod user details");
            }*/
        //  }

        usernamee.setText("Dobro došli " + username + "!");

        backToH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
            }
        });

        lobtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.commit();
                finish();
                Toast.makeText(UserDetails.this, "Odjavljeni ste", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });


        commList=DBcom.getCommData(username);
        if(commList!=null) {
            linearLayoutManager = new LinearLayoutManager(this);
            commView.setLayoutManager(linearLayoutManager);
            adapter = new CommentsAdapter(UserDetails.this, commList);
            commView.setAdapter(adapter);
            addBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int idTXT = comm.getId();//
                    String aboutTXT = comm.getText().toString();
                    if (!aboutTXT.equals("")) {
                        Boolean checkinsertdata = DBcom.insertComm(idTXT, aboutTXT, username);


                        if (checkinsertdata) {
                            comm.setText("");
                            Toast.makeText(UserDetails.this, "Uspjesno dodan komentar u bazu", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(UserDetails.this, "Neuspjesno dodavanje komentara u bazu", Toast.LENGTH_SHORT).show();
                        }
                        commList.clear();
                        commList.addAll(DBcom.getCommData(username));
                        adapter.notifyDataSetChanged();
                    }
                }
            });
        }
    }





    private void setFavsView(List<FavoritesData> favsDataList){  //?
        favs=findViewById(R.id.favoriti_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        favs.setHasFixedSize(true); //idk
        favs.setLayoutManager(layoutManager);
        favAdapter= new FavoriteAdapter(this, favsDataList);
        favs.setAdapter(favAdapter);
        favAdapter.notifyDataSetChanged();
    }

    private void loadData(){
        if(favsList !=null)
            favsList.clear();
        SQLiteDatabase mydb=DB.getReadableDatabase();
        Cursor cursor=DB.selectAllFavoriteList();
        try {
            while(cursor.moveToNext()){
                String id=cursor.getString(cursor.getColumnIndex(DBFavHelper.KEY_ID));
                String title=cursor.getString(cursor.getColumnIndex(DBFavHelper.ITEM_TITLE));
                String about =cursor.getString(cursor.getColumnIndex(DBFavHelper.ITEM_ABOUT));

                int image=Integer.parseInt(cursor.getString(cursor.getColumnIndex(DBFavHelper.ITEM_IMAGE)));
                FavoritesData favitem=new FavoritesData(id,title,about,image,"1");

                favsList.add(favitem);
            }
        }finally {
            if(cursor !=null && cursor.isClosed())
                cursor.close();
            mydb.close();
        }
        favAdapter=new FavoriteAdapter(this,
                favsList);
        favs.setAdapter(favAdapter);

    }
}
