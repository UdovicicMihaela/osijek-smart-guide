package ferit.mihaelaudovicic.osijeksmartguide;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import ferit.mihaelaudovicic.osijeksmartguide.model.CommentData;

public class DBCommentHelper extends SQLiteOpenHelper {
    public DBCommentHelper(@Nullable Context context) {
        super(context, "CommDB.db", null, 1);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table comments(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, about TEXT, username TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists comments");
    }
    public boolean insertComm(Integer id,String text, String username){
        SQLiteDatabase DB=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        //contentValues.put("id",id);
        contentValues.put("about",text);
        contentValues.put("username",username);
        long result=DB.insert("comments",null,contentValues);
        if (result==-1){
            return false;
        }else{
            return true;
        }
    }
    public boolean updateComm(Integer id, String text, String username){
        SQLiteDatabase DB=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();

        //contentValues.put("id",id);
        contentValues.put("about",text);
        //contentValues.put("username",username);

        long result = DB.update("comments", contentValues, "id = "+id,null);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
        /*
        Cursor cursor=DB.rawQuery("select * from comments where id = "+id,null);//new String[]{text});
        if (cursor.getCount() > 0) {
            long result = DB.update("comments", contentValues, "id = "+id,null);
            if (result == -1) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }*/
    }
    public boolean deleteComm(String text){
        SQLiteDatabase DB=this.getWritableDatabase();
        Cursor cursor=DB.rawQuery("select * from comments where about = ?",new String[]{text});
        if (cursor.getCount() > 0) {
            long result = DB.delete("comments", "text=?", new String[]{text});
            if (result == -1) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }
    }

    public boolean deleteD(CommentData cd){
        SQLiteDatabase DB=this.getWritableDatabase();

        Cursor cursor=DB.rawQuery("select * from comments where about = ?",new String[]{cd.getTextComment()});
        if (cursor.getCount() > 0) {
            long result = DB.delete("comments", "about=?", new String[]{cd.getTextComment()});
            if (result == -1) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }
    }


    public Cursor getData(){
        SQLiteDatabase DB=this.getWritableDatabase();
        Cursor cursor=DB.rawQuery("select * from comments",null);
        return cursor;
    }
    public List<CommentData> getCommData(String username){
        SQLiteDatabase DB=this.getWritableDatabase();
        List<CommentData> cd=new ArrayList<>();
        Cursor cursor=DB.rawQuery("select * from comments where username = ?",new String[]{username},null);
        if(cursor!=null) {
            while (cursor.moveToNext()) {
                CommentData commd = new CommentData();
                commd.setId(getIntByColumName(cursor, "id"));
                commd.setTextComment(getStringByColumName(cursor, "about"));
                cd.add(commd);
            }
        }
        cursor.close();
        DB.close();
        return cd;
    }
    public int getIntByColumName(Cursor cursor, String tableColumn) {
        return cursor.getInt(cursor.getColumnIndex(tableColumn));
    }
    public String getStringByColumName(Cursor cursor, String tableColumn) {
        return cursor.getString(cursor.getColumnIndex(tableColumn));
    }
}
